#!/bin/bash

read -p "Enter the remote name : " remote
if [ ! -n "$remote" ]; then
    remote="origin"
fi

read -p "Enter the branch : " branch
if [ ! -n "$branch" ]; then
    branch="master"
fi

command="git pull ${remote} ${branch}"
echo -e "The command is :\n\n${command}\n"

read -p "do you want to execute the command?[y/n] " result
if [ "$result" == "y" -o "$result" == "Y" ]; then
    $command
else
    echo "quit"
fi
