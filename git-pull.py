#!/usr/bin/env python
# -*_ coding: utf-8 -*-

import os
from config import env
from termstyle import *

def get_dir_list():
    git_dir_list = []
    for path in os.listdir(env.ROOT):
        if (os.path.isdir(env.ROOT + '/' + path)):
            git_dir_list.append(path)
    return git_dir_list

def check_git_dir(dirname):
    return '.git' in os.listdir(dirname)

def change_dir(dirname):
    os.chdir(dirname)

def update():
    os.system('git pull origin master')

if __name__ == '__main__':
    dir_list = get_dir_list()
    for dirname in dir_list:
        dirname = env.ROOT + '/' + dirname
        print('Change To [%s]' % blue(dirname))
        if check_git_dir(dirname):
            change_dir(dirname)
            update()
        else:
            print(red('This is NOT a target directory'))
        print(green('[%s] Update Finished!!\n' % dirname))

