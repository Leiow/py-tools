#!/bin/bash

if [ ! -n "$1" ]; then
    echo "missing username"
    exit 1
fi
if [ ! -n "$2" ]; then
    echo "missing server address"
    exit 1
fi
ssh-add
ssh-agent
ssh -A $1@$2
